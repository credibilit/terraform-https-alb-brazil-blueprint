resource "aws_alb" "alb" {
  name            = "${var.name}"
  internal        = "${var.internal}"
  security_groups = [ "${aws_security_group.alb.id}" ]
  access_logs {
    bucket = "${module.s3_bucket_access_log.id}"
    prefix = ""
  }
  subnets                    = [ "${var.subnets}" ]
  idle_timeout               = "${var.idle_timeout}"
  enable_deletion_protection = "${var.enable_deletion_protection}"
  tags                       = "${var.tags}"
}

output "alb" {
  value = {
    id         = "${aws_alb.alb.id}"
    arn        = "${aws_alb.alb.arn}"
    arn_suffix = "${aws_alb.alb.arn_suffix}"
    dns_name   = "${aws_alb.alb.dns_name}"
    zone_id    = "${aws_alb.alb.zone_id}"
  }
}
