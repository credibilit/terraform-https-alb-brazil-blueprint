resource "aws_alb_target_group" "target_group" {
  // Required
  name     = "${var.target_group_name}"
  port     = "${var.target_group_port}"
  protocol = "${var.target_group_protocol}"
  vpc_id   = "${data.aws_subnet.subnet.vpc_id}"

  // Optional
  deregistration_delay = "${var.target_group_deregistration_delay}"
  health_check {
    interval            = "${var.target_group_health_check_interval}"
    path                = "${var.target_group_health_check_path}"
    port                = "${var.target_group_health_check_port}"
    protocol            = "${var.target_group_health_check_protocol}"
    timeout             = "${var.target_group_health_check_timeout}"
    healthy_threshold   = "${var.target_group_health_check_healthy_threshold}"
    unhealthy_threshold = "${var.target_group_health_check_unhealthy_threshold}"
    matcher             = "${var.target_group_health_check_matcher}"
  }
  tags = "${var.tags}"
}

output "target_group" {
  value = {
    id         = "${aws_alb_target_group.target_group.id}" 
    arn        = "${aws_alb_target_group.target_group.arn}" 
    arn_suffix = "${aws_alb_target_group.target_group.arn_suffix}" 
  }
}
