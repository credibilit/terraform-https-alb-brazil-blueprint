variable "account" {
  default = "495770326048"
}

variable "name" {
  default = "alb-acme"
}

// ALB test
module "test" {
  source  = "../"
  account = "${var.account}"

  name     = "${var.name}"
  subnets  = "${module.vpc_test.public_subnets}"

  target_group_name = "web"
  target_group_port = 80

  // Optional
  internal = false
  idle_timeout = 60
  tags = {
    test = "${var.name}"
  }

  certificate_arn = "${aws_iam_server_certificate.acme.arn}"
}

resource "aws_alb_target_group_attachment" "test" {
  target_group_arn = "${lookup(module.test.target_group, "arn")}"
  target_id        = "${aws_instance.web.id}"
  port             = 80
}

output "test" {
  value = {
    security_group = "${module.test.security_group}"
    alb            = "${module.test.alb}"
    target_group   = "${module.test.target_group}"
    listener       = "${module.test.listener}"
  }
}
