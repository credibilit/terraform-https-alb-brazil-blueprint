// VPC
module "vpc_test" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.3"
  account = "${var.account}"

  name                      = "${var.name}"
  cidr_block                = "10.110.0.0/16"
  public_subnets_cidr_block = [ "10.110.1.0/24", "10.110.2.0/24" ]
  azs                       = [ "us-east-1a", "us-east-1b" ]
  az_count                  = 2
  domain_name               = "${var.name}.test"
  hosted_zone_comment       = "An internal hosted zone for ${var.name} vpc"
}

resource "aws_security_group_rule" "alb_access" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${lookup(module.test.security_group, "id")}"
}

resource "aws_iam_server_certificate" "acme" {
  name             = "${var.name}"
  certificate_body = "${file("keys/acme.com.crt")}"
  private_key      = "${file("keys/acme.com.key")}"
}
