resource "aws_alb_listener" "listener_http" {
  depends_on        = [ "aws_alb_target_group.target_group" ]
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "${var.listener_port_http}"
  protocol          = "HTTP"
  default_action {
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
    type             = "forward"
  }
}

output "listener_http" {
  value = {
    id  = "${aws_alb_listener.listener_http.id}"
    arn = "${aws_alb_listener.listener_http.arn}"
  }
}

resource "aws_alb_listener" "listener_https" {
  depends_on        = [ "aws_alb_target_group.target_group" ]
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "${var.listener_port_https}"
  protocol          = "HTTPS"
  default_action {
    target_group_arn = "${aws_alb_target_group.target_group.arn}"
    type             = "forward"
  }
  ssl_policy      = "ELBSecurityPolicy-2015-05"
  certificate_arn = "${var.certificate_arn}"
}

output "listener_https" {
  value = {
    id  = "${aws_alb_listener.listener_https.id}"
    arn = "${aws_alb_listener.listener_https.arn}"
  }
}

# TODO: add more rules
# resource "aws_alb_listener_rule" "rule" {
#   listener_arn = "${aws_alb_listener.listener.arn}"
#   priority     = "${var.listener_rule_priority}"
#   action {
#     type = "forward"
#     target_group_arn = "${aws_alb_target_group.target_group.arn}"
#   }
#   condition {
#     field = "path-pattern"
#     values = [ "${var.listener_rule_condition_values}" ]
#   }
# }
