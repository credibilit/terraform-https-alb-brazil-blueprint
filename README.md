Generic ALB Terraform Module
============================

# Use

To create an ALB with this module, you need to insert the following peace of code on your own modules:

```
// Call the module
module "alb" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-https-alb-brazil-blueprint.git?ref=<VERSION>"

  name     = "${var.name}"
  subnets  = "${module.vpc_test.public_subnets}"

  target_group_name = "web"
  target_group_port = 80

  // Optional
  internal = false
  idle_timeout = 60
  tags = {
    test = "${var.name}"
  }

  certificate_arn = "${aws_iam_server_certificate.acme.arn}"
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Input parameters

- `account`: The account number ID
- `name`: A name for the ALB and related elements
- `internal`: If true, the ALB will be internal
- `subnets`: A list of subnet IDs to attach to the ALB
- `idle_timeout`: The time in seconds that the connection is allowed to be idle. Default: 60
- `enable_deletion_protection`: If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer
- `tags`: A mapping of tags to assign to the resource
- `s3_bucket_force_destroy`: If the bucket must destroy all objects inside the bucket before delete the bucket.
- `lifecycle_rule_days_sia`: How many days the objects will stay in default storage area before go to SIA. (default: 30 days)
- `lifecycle_rule_days_glacier`: How many days the objects will stay in SIA storeage area before go to Glacier. (default: 90 days)
- `lifecycle_rule_expiration`: In how many days the objects will be deleted. (default: 365 days)
- `target_group_name`: The name of the target group.
- `target_group_port`: The port on which targets receive traffic, unless overridden when registering a specific target
- `target_group_protocol`: The protocol to use for routing traffic to the targets. Default: HTTP
- `target_group_deregistration_delay`: The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds. Default: 300
- `target_group_stickiness_type`: The type of sticky sessions. The only current possible value is lb_cookie
- `target_group_stickiness_cookie_duration`: The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale
- `target_group_health_check_interval`: The approximate amount of time, in seconds, between health checks of an individual target
- `target_group_health_check_path`: The destination for the health check request
- `target_group_health_check_port`: The port to use to connect with the target. Valid values are either ports 1-65536, or traffic-port
- `target_group_health_check_protocol`: The protocol to use to connect with the target
- `target_group_health_check_timeout`: The amount of time, in seconds, during which no response means a failed health check
- `target_group_health_check_healthy_threshold`: The number of consecutive health checks successes required before considering an unhealthy target healthy
- `target_group_health_check_unhealthy_threshold`: The number of consecutive health check failures required before considering the target unhealthy
- `target_group_health_check_matcher`: The HTTP codes to use when checking for a successful response from a target
- `listener_port_http`: The port HTTP on which the load balancer is listening
- `listener_port_https`: The port HTTPS on which the load balancer is listening
- `certificate_arn`: The ARN of the SSL server certificate. Exactly one certificate is required if the protocol is HTTPS
- `listener_rule_priority`: The priority for the rule. A listener can't have multiple rules with the same priority
- `listener_rule_condition_values`: The path patterns to match


## Output parameters

- `listener`
- `target_group`
- `alb`
- `s3_bucket_access_log`
- `security_group`
