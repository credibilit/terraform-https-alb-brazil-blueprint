variable "account" { }

variable "name" {
  description = "A name for the ALB and related elements"
}

variable "internal" {
  description = "If true, the ALB will be internal"
  default = false
}

variable "subnets" {
  type = "list"
  description = "A list of subnet IDs to attach to the ALB"
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle. Default: 60"
  default = 60
}

variable "enable_deletion_protection" {
  description = "If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer"
  default = false
}

variable "tags" {
  type = "map"
  description = "A mapping of tags to assign to the resource"
  default = {}
}

// S3 bucket logs
variable "s3_bucket_force_destroy" {
  description = "If the bucket must destroy all objects inside the bucket before delete the bucket."
  default = true
}

variable "lifecycle_rule_days_sia" {
  description = "How many days the objects will stay in default storage area before go to SIA. (default: 30 days)"
  default = 30
}

variable "lifecycle_rule_expiration" {
  description = "In how many days the objects will be deleted. (default: 365 days)"
  default = 365
}

// Target Group
variable "target_group_name" {
  description = "The name of the target group."
}

variable "target_group_port" {
  description = "The port on which targets receive traffic, unless overridden when registering a specific target"
}

variable "target_group_protocol" {
  description = "The protocol to use for routing traffic to the targets. Default: HTTP"
  default = "HTTP"
}

variable "target_group_deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds. Default: 300"
  default = 300
}

variable "target_group_health_check_interval" {
  description = "The approximate amount of time, in seconds, between health checks of an individual target"
  default = 30
}

variable "target_group_health_check_path" {
  description = "The destination for the health check request"
  default = "/"
}

variable "target_group_health_check_port" {
  description = "The port to use to connect with the target. Valid values are either ports 1-65536, or traffic-port"
  default = "traffic-port"
}

variable "target_group_health_check_protocol" {
  description = "The protocol to use to connect with the target"
  default = "HTTP"
}

variable "target_group_health_check_timeout" {
  description = "The amount of time, in seconds, during which no response means a failed health check"
  default = 5
}

variable "target_group_health_check_healthy_threshold" {
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy"
  default = 5
}

variable "target_group_health_check_unhealthy_threshold" {
  description = "The number of consecutive health check failures required before considering the target unhealthy"
  default = 2
}

variable "target_group_health_check_matcher" {
  description = "The HTTP codes to use when checking for a successful response from a target"
  default = 200
}

// ALB Listener
variable "listener_port_http" {
  description = "The port on which the load balancer is listening"
  default = 80
}

variable "listener_port_https" {
  description = "The port on which the load balancer is listening"
  default = 443
}

variable "certificate_arn" {
  description = "The ARN of the SSL server certificate. Exactly one certificate is required if the protocol is HTTPS"
}

variable "listener_rule_priority" {
  description = "The priority for the rule. A listener can't have multiple rules with the same priority"
  default = 100
}

variable "listener_rule_condition_values" {
  description = "The path patterns to match"
  default = "/"
}
